// импорты
#include <cstring>
#include <iostream>
#include "Summoner.h"


// конструктор
Summoner::Summoner(
    const char* name,
    uint16_t level,
    uint32_t hp,
    uint32_t max_hp,
    double gold,
    uint32_t mana,
    uint32_t max_mana,
    const char* creature
) : Mage(name, level, hp, max_hp, gold, mana, max_mana) {
    strcpy(this->creature, creature);
    classname = SUMMONER;
}


// конструктор по умолчанию
Summoner::Summoner() {
    classname = SUMMONER;
    strcpy(creature, "Golem");
}


// метод печати текстового описания
void Summoner::print(std::ostream& ostream) const {
    Mage::print(ostream);
    ostream << ", creature=" << creature << std::endl;
}


// запись в файл
void Summoner::save(std::ofstream& fout) {
    Mage::save(fout);
    fout.write(reinterpret_cast<char*>(creature), sizeof(creature));
}


// загрузка из файла
void Summoner::load(std::ifstream& fin) {
    Mage::load(fin);
    fin.read(reinterpret_cast<char*>(creature), sizeof(creature));
}


// геттер призываемого существа
const char* Summoner::get_creature() const {
    return creature;
}
