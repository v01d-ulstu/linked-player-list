#pragma once

#include "config.h"
#include "Warrior.h"


class Ranged : public Warrior {
protected:
    float accuracy;
public:
    Ranged(const char*, uint16_t, uint32_t, uint32_t, double, uint32_t, float);
    Ranged();

    void print(std::ostream&) const;
    void save(std::ofstream&);
    void load(std::ifstream&);

    float get_accuracy() const;
};
