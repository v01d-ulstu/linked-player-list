// импорты
#include <iostream>
#include <cstring>

#include "Mage.h"


// Конструктор
Mage::Mage(
    const char* name,
    uint16_t level,
    uint32_t hp,
    uint32_t max_hp,
    double gold,
    uint32_t mana,
    uint32_t max_mana
) : BasePlayer(name, level, hp, max_hp, gold), mana(mana), max_mana(max_mana) {
    // указание типа
    classname = MAGE;
}


// Конструктор по умолчанию
Mage::Mage() : mana(10), max_mana(10) {
    classname = MAGE;
}


// метод печати текстового описания
void Mage::print(std::ostream& ostream) const {
    // вызов родительского метода
    BasePlayer::print(ostream);
    ostream << ", mana=" << mana << "/" << max_mana;
};


// запись в файл
void Mage::save(std::ofstream& fout) {
    BasePlayer::save(fout);
    // запись в байтовом виде
    fout.write(reinterpret_cast<char*>(&mana), sizeof(mana));
    fout.write(reinterpret_cast<char*>(&max_mana), sizeof(max_mana));
}

// загрузка из файла
void Mage::load(std::ifstream& fin) {
    BasePlayer::load(fin);
    fin.read(reinterpret_cast<char*>(&mana), sizeof(mana));
    fin.read(reinterpret_cast<char*>(&max_mana), sizeof(max_mana));
}


// геттер маны
uint32_t Mage::get_mana() const {
    return mana;
}


// геттер макс. маны
uint32_t Mage::get_max_mana() const {
    return max_mana;
}
