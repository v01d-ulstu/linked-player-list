#pragma once
#include <cstdint>

#include "BasePlayer.h"
#include "config.h"


class Mage : public BasePlayer {
protected:
    uint32_t mana;
    uint32_t max_mana;
public:
    Mage(const char*, uint16_t, uint32_t, uint32_t, double, uint32_t, uint32_t);
    Mage();

    virtual void print(std::ostream&) const;
    virtual void save(std::ofstream&);
    virtual void load(std::ifstream&);

    uint32_t get_mana() const;
    uint32_t get_max_mana() const;
};
