#pragma once

#include "config.h"
#include "BasePlayer.h"


class Warrior : public BasePlayer {
protected:
    uint32_t strength;
public:
    Warrior(const char*, uint16_t, uint32_t, uint32_t, double, uint32_t);
    Warrior();

    virtual void print(std::ostream&) const;
    virtual void save(std::ofstream&);
    virtual void load(std::ifstream&);

    uint32_t get_strength() const;
};
