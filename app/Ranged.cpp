#include <iostream>
#include <cstring>

#include "Ranged.h"


Ranged::Ranged(
    const char* name,
    uint16_t level,
    uint32_t hp,
    uint32_t max_hp,
    double gold,
    uint32_t strength,
    float accuracy
) : Warrior(name, level, hp, max_hp, gold, strength), accuracy(accuracy) {
    classname = RANGED;
}


Ranged::Ranged() : accuracy(0.5) {
    classname = RANGED;
}


void Ranged::print(std::ostream& ostream) const {
    Warrior::print(ostream);
    ostream << ", accuracy=" << accuracy << std::endl;
}


void Ranged::save(std::ofstream& fout) {
    Warrior::save(fout);
    fout.write(reinterpret_cast<char*>(&accuracy), sizeof(accuracy));
}


void Ranged::load(std::ifstream& fin) {
    Warrior::load(fin);
    fin.read(reinterpret_cast<char*>(&accuracy), sizeof(accuracy));
}


float Ranged::get_accuracy() const {
    return accuracy;
}
