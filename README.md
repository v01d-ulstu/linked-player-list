# Linked Player List

## How To Build And Run

### On Windows

Use `scoop` to install `gcc`, `make` and `cmake`.

Create make-files

```ps1
cmake -G "Unix Makefiles" -S . -B build
```

Build

```ps1
make -C build
```

Run

```ps1
.\build\linked_player_list.exe
```
